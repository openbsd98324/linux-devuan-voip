# linux-devuan-voip

Linux Devuan Live for VOIP and video conferencing 


![](https://gitlab.com/openbsd98324/linux-devuan-voip/-/raw/master/desktop.png)

![](https://gitlab.com/openbsd98324/linux-devuan-voip/-/raw/master/desktop1.png)


# Content

The partition 1 is of type ef with a grub boot loader.
The partition 2 is NetBSD with a kernel running out of ram and it has the active partition with the grub boot loader.
The partition 3 is the filesystem running on the memory, i.e. ramdisk, using overlay.

The system will give you all tools and utils for video conferencing.
It runs devuan for high quality OS and high security. The source code is available at devuan.org and into /usr/src.

It has numerous tools for telephony and distant usage, like x11vnc, xtightvncviewer, ssh, revssh, ekiga for voip, skype (non-free, widely available),... and many more.

Have Fun!


# Download

In Directory v2, fetch:

  image-linux-devuan-voip-v2.img.gz 

  the md5 is : 
  
  
  ec0e63dd99cc0c91005815479e15d8ba  image-linux-devuan-voip-v2.img

  689ea2ad4be9fb8db2d3af298b4b047c  image-linux-devuan-voip-v2.img.gz



# Preparation of the pendrive

The Linux VOIP (DEVUAN) system will run as a Live (into the RAM) of your PC.
You will need a system like OpenBSD, NetBSD, Linux,... to prepare your pendrive.
Alternatively, Rawrite is given to create your pendrive live with the VOIP system.

Furthermore, the image can be used as a full linux operating system, and it can be copied on a hardisk. 
The GRUB2 menu needs then to be adapted.

A pendrive with a size greater than 2GB is required. 

Ideally, if you have a PC with 8GB, this is fair sufficient to run a great number of applications into the memory as live.  



# Pendrive Live

To copy the image on a pendrive:

zcat  image-linux-devuan-voip-v2.img.gz  >   /dev/sdb   


If you have windows, use Rawrite32 to copy the image onto your harddisk or memstick.

# Running Linux VOIP

Once you have prepared the pendrive with the live operating system (linux devuan, with desktop, and available software), you put the pendrive into the usb port of your PC/notebook, and press the F12 (F9,... depending on boot/bios of PC) to select the usb pendrive.

Once the pendrive is started, the GRUB2 menu will appear. Then, you may select the sda2 with the linux voip devuan system.
(depending on your hardware, sda2 can be adapted)

 ![](media/devuan-linux-voip-example-notebook-1.png)

 


# Making a VOIP SIP Phone Call

Right click on the desktop, and select xterm.

type:   ekiga

Then, you may make a VOIP phone call. 

Example to make a SIP phone call directly to a given IP address:  sip:38.XX.XX.XX   



# Setting my MIC/AUDIO


Right click on the desktop, and select xterm.

type: 
pavucontrol


# Linphone 

For a greater experience, you may also run : Linphone.
Linphone is a free, open VOIP software. More features are available like accounts.

dhclient eth0

apt-get update

apt-get install linphone 

# Have Fun with VOIP 
